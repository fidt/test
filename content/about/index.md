+++
title = '关于'
date = '2022-10-21T20:36:39+08:00'
+++

<span id="Chinese"></span>

## [[English]](#English)

<img src="../pic/logo.png" width=150 alt="FIDT logo"></img>

# FIDT / 世界井字棋协会 

**(Fédération Internationale des Tic-tac-toe)**

**International Tic-tac-toe Federation**

---------------------


井字棋
-----
井字棋，为人们所熟知，是一种规则简单的零和博弈棋类游戏。“井字棋”因其棋盘类似“井”字而得名。井字棋通常有两种棋子：`O`和`X`。对弈双方在方寸之间着此二棋大显身手。

FIDT介绍
--------
> FIDT（Fédération Internationale des Tic-tac-toe 世界井字棋协会）是制定通用井字棋标准，维护井字棋比赛秩序，维护棋手合法权益的组织。

**网站:** 
+ [https://maxchen32.github.io/fidt](https://maxchen32.github.io/fidt "FIDT网站")
+ [https://fidt.codeberg.page](https://fidt.codeberg.page "FIDT网站")

FIDT网站主要内容
--------------
 1. FIDT公告
 2. 井字棋赛事新闻
 3. 井字棋文化
 
<b style="color: red">
    <span style="color:#ff0000;">F</span>
    <span style="color:#0000ff;">I</span>
    <span style="color:#00ff00;">D</span>
    <span style="color:#ff00ff;">T</span>及网站内容纯属虚构
</b>


<span id="English"></span>

## [[中文]](#Chinese)

<img src="../pic/logo.png" width=150 alt="FIDT logo"></img>

# FIDT / International Tic-tac-toe Federation

**(Fédération Internationale des Tic-tac-toe)**

**世界井字棋协会**

---------------------


Tic-Tac-Toe
-----------
Tic-tac-toe, well known to people, is a zero-sum board game with simple rules. Tic-Tac-Toe players use two symbols - circles `O` and crosses `X` - for a contest of intelligence and skills in a boxlike figure.

FIDT Introduction
-----------------
> FIDT（Fédération Internationale des Tic-tac-toe / International Tic-tac-toe Federation） is an organization that formulates common Tic-tac-toe standards, maintains the order of Tic-tac-toe games, and protects the legitimate rights and interests of players.

**Website:**
+ [https://maxchen32.github.io/fidt](https://maxchen32.github.io/fidt "FIDT Website")
+ [https://fidt.codeberg.page](https://fidt.codeberg.page "FIDT Website")

Main Content of FIDT Website
----------------------------
 1. FIDT Announcement
 2. Tic-tac-toe Event News
 3. Tic-Tac-Toe Culture
 
<strong style="color: red">
                <span style="color:#ff0000;">F</span>
                <span style="color:#0000ff;">I</span>
                <span style="color:#00ff00;">D</span>
                <span style="color:#ff00ff;">T</span> and website content are fictitious
            </strong>
