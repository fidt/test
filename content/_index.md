+++

+++
<div style="text-align:center;margin:0">
	<!--<a href="https://maxchen32.github.io/fidt" title="FIDT-首页">
		<img style="width:25%" class="biglogo" src="./pic/logo.png" alt="FIDT_logo"></a>
	!-->
	<b>
	<span style="font-style:italic;font-size:2.5em;font-family:sans-serif;margin:2px;">
		<span style="color:#ff0000;margin:inherit;">F</span><span style="color:#0000ff;margin:inherit;">I</span><span style="color:#00ff00;margin:inherit;">D</span><span style="color:#ff00ff;margin:inherit;">T</span>
	</span>
	<span style="color: #000;">世界井字棋协会<br />Fédération Internationale des Tic-tac-toe<br />International Tic-tac-toe Federation</span>
	</b>
</div>

## 井字棋训练
[Jogo da Velha - Minimax](https://cledersonbc.github.io/tic-tac-toe-minimax)

## 新闻
[#新闻](./tags/新闻.html)

## 文化集萃
[#文化集萃](./tags/文化集萃.html)


<!--
```goat
 +------+-------+------+
+       |       | --+-- +
|       |       |   |   |
|       |       | --+-- |
+-------+-------+-------+
|       |  +--- |       |
|       | +---  |       |
|       | +     |       |
+-------+-------+-------+
| +--+  |       | --+-- |
| |   + |       |   |   |
+ '--+  |       |   +   +
 +------+-------+------+
```
!-->